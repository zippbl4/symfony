<?php

namespace App\ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class QueryStringConverter implements ParamConverterInterface
{
    public function supports(ParamConverter $configuration)
    {
        return 'querystring' == $configuration->getConverter();
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $normalizers = [
            new ObjectNormalizer(null, null, null, new PhpDocExtractor()),
            new ArrayDenormalizer(),
        ];

        $object = (new Serializer($normalizers))->denormalize(
            $request->query->all(),
            $configuration->getClass()
        );

        $request->attributes->set(
            $configuration->getName(),
            $object
        );
    }
}