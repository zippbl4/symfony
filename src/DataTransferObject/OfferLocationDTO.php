<?php

namespace App\DataTransferObject;

class OfferLocationDTO
{
    private string $place_id;

    private string $supplier_name;

    /**
     * @return string
     */
    public function getPlaceId(): string
    {
        return $this->place_id;
    }

    /**
     * @param string $placeId
     */
    public function setPlaceId(string $placeId): void
    {
        $this->place_id = $placeId;
    }

    /**
     * @return string
     */
    public function getSupplierName(): string
    {
        return $this->supplier_name;
    }

    /**
     * @param string $supplierName
     */
    public function setSupplierName(string $supplierName): void
    {
        $this->supplier_name = $supplierName;
    }
}