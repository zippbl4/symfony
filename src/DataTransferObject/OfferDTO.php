<?php

namespace App\DataTransferObject;

class OfferDTO
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var OfferLocationDTO[]
     */
    private array $from;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from): void
    {
        $this->from = $from;
    }
}