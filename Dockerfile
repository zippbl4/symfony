FROM php:7.4.0-fpm-alpine3.10

RUN apk add --no-cache \
    git \
    curl \
    bash \
    nano \
    $PHPIZE_DEPS \
    # libzip \
    libzip-dev \
    # libxml2 \
    libxml2-dev \
    icu \
    icu-dev

RUN git config --global user.email "you@example.com" &&\
    git config --global user.name "example"

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
RUN curl https://get.symfony.com/cli/installer -o - | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

RUN docker-php-ext-install \
    pdo \
    pdo_mysql \
    intl \
    zip \
    soap \
    bcmath \
    exif

RUN mkdir -p /var/www/

WORKDIR /var/www/
